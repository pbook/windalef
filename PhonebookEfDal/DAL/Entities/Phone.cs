﻿namespace DAL.Entities
{
    public class Phone : BaseEntity
    {
        public int ContactId { get; set; }
        public string Number { get; set; }

        public virtual Contact Contact { get; set; }
    }
}
