﻿using System.Collections.Generic;

namespace DAL.Entities
{
    public class Contact : BaseEntity
    {
        public string Name { get; set; }
        public string Email { get; set; }

        public virtual List<Phone> Phones { get; set; }
    }
}
