﻿using DAL.Entities;
using System.Data.Entity;

namespace DAL
{
    public class PhonebookContext : DbContext
    {
        public PhonebookContext() : base("PhonebookDalEf")
        {
            var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Phone> Phones { get; set; }
    }
}
