﻿using DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace DAL.Repository
{
    public class BaseRepository<T> where T : BaseEntity
    {
        private DbContext db;
        private DbSet<T> dbSet;

        public BaseRepository()
        {
            db = new PhonebookContext();
            dbSet = db.Set<T>();
        }

        public virtual List<T> GetAll()
        {
            return dbSet.Where(e => e.IsDeleted == false).ToList();
        }

        public virtual List<T> GetAll(Expression<Func<T, bool>> filter)
        {
            return dbSet.Where(filter).ToList();
        }

        public virtual T GetById(int id)
        {
            return dbSet.Find(id);
        }

        public virtual T Get(Expression<Func<T, bool>> filter)
        {
            return dbSet.FirstOrDefault(filter);
        }

        public virtual void Insert(T entity)
        {
            entity.CreatedAt = DateTime.Now;
            entity.UpdatedAt = DateTime.Now;

            dbSet.Add(entity);
            db.SaveChanges();
        }

        public virtual void Update(T entity)
        {
            entity.UpdatedAt = DateTime.Now;

            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }

        public virtual void Delete(T entity)
        {
            entity.UpdatedAt = DateTime.Now;
            entity.IsDeleted = true;

            db.Entry(entity).State = EntityState.Modified;
            db.SaveChanges();
        }
    }
}
