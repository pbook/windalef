﻿using DAL.Entities;
using System;
using System.Windows.Forms;

namespace PhonebookEfDal
{
    public partial class FormAddEditPhone : Form
    {
        private Phone phone;

        public FormAddEditPhone(Phone phone)
        {
            InitializeComponent();
            this.phone = phone;
        }

        private void FormAddEditPhone_Load(object sender, EventArgs e)
        {
            this.Text = string.Format("{0} Phone - Phonebook", phone.Id > 0 ? "Update" : "Add");

            textBoxNumber.Text = phone.Number;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            phone.Number = textBoxNumber.Text;

            this.DialogResult = DialogResult.OK;
        }
    }
}
